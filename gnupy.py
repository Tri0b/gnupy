#!/usr/bin/env python

import argparse
import sys
from src.settings import Settings
from src.main import main


def cli():
    parser = argparse.ArgumentParser(
        description="Create latex figures from pre-formated csv files",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("files", help="CSV file list", nargs="*")
    parser.add_argument(
        "-i",
        "--ignore",
        dest="ignore",
        nargs="+",
        type=int,
        help="Column index to ignore (1 is the first column).",
    )
    parser.add_argument(
        "-o", "--output", dest="outputDir", help="Output directory", default="."
    )
    parser.add_argument(
        "-f",
        "--format",
        dest="format",
        help="Output format (tex, png, jpg, svg).",
        default="png",
    )
    parser.add_argument(
        "--delimiter",
        dest="delimiter",
        help="Delimiter character used in the csv files",
        default=";",
    )
    # parser.add_argument(
    # "--quote",
    # dest="quote",
    # help="Quote character used in the csv files",
    # default="'",
    # )
    parser.add_argument(
        "-c",
        "--columns",
        dest="columns",
        nargs="+",
        help="Select columns to plot. (1 is the first column). By default all columns are plotted.",
    )
    parser.add_argument(
        "-x",
        "--xaxis",
        dest="xaxis",
        default=1,
        type=int,
        help="Select the column used for x axis",
    )
    parser.add_argument(
        "-t",
        "--themes",
        dest="theme",
        help="Set the color scheme",
        default="gruvbox",
    )
    parser.add_argument(
        "--list-themes",
        dest="listThemes",
        help="List the themes available",
        action="store_true",
    )
    parser.add_argument(
        "--script-only",
        dest="scriptOnly",
        default=False,
        help="Script only, the gnuplot script is not executed.",
        action="store_true",
    )
    parser.add_argument(
        "--keep-script",
        dest="keepScript",
        default=False,
        help="Keep the gnuplot script.",
        action="store_true",
    )
    parser.add_argument(
        "-g", "--gnuargs", dest="gnuargs", help="Extra arguments for gnuplot"
    )
    args = parser.parse_args()

    return args, parser


if __name__ == "__main__":
    args, parser = cli()
    settings = Settings()
    settings.setTheme(args.theme)
    settings.setIgnore(args.ignore)
    settings.setColumns(args.columns)
    settings.setXAxis(args.xaxis)
    settings.setOutputDir(args.outputDir)
    settings.setDelimiter(args.delimiter)
    settings.setFormat(args.format)
    settings.setFiles(args.files)

    if args.listThemes:
        # TODO: Use rich to show sample
        print(settings.getThemeList())
        sys.exit(0)

    if args.scriptOnly:
        settings.generateScript()
        sys.exit(0)

    if args.keepScript:
        settings.keepScript = True

    main(settings)
