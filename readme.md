# Gnupy : When you just want to plot

## Description

[Gnuplot](http://www.gnuplot.info/documentation.html) is a very versatile and powerful tool, but as powerful as it is, plotting the graph you want 
may not be straightforward. 
Gnupy is a simple python script built on top of gnuplot to make it easier to use.

## Dependencies 
You need gnuplot installed and loaded in your path, refer to your package manager.
```bash
sudo pacman -S gnuplot
```
```bash
sudo apt install gnuplot
```

Tested on Python 3.10 and gnuplot 5.4.
No non-standard module required.

## Usage

You need a csv formatted file.
The data must starts on the first row, the file may or may not have column headers.


Plot all columns found in `timewaveforms.csv`.
```bash
./gnupy data/timewaveform.csv -o yourOutputDir
```

You can specify the output format, the theme
```bash
./gnupy data/* -o OutputDir -f tex -t sharpattack
```

Ignore some columns you don't want to plot

```bash
./gnupy data/timewaveform.csv -i 2 3 -o yourOutputDir
```


## Latex usage
WIP

## Custom themes
Adding your own theme is quiet straightforward, just have a look to the existing themes i.e `themes/gruvbox.json`.


## Gallery
### Themes
#### Gruvbox
```bash
./gnupy data/timewaveform.csv -t gruvbox
```
![Gruvbox](ressources/gruvbox.png)

#### Monokai
```bash
./gnupy data/timewaveform.csv -t monokai
```
![monokai](ressources/monokai.png)


#### Classic Dark
```bash
./gnupy data/timewaveform.csv -t classic-dark
```
![classic-dark](ressources/classic-dark.png)



#### Classic Light
```bash
./gnupy data/timewaveform.csv -t classic-light
```
![classic-light](ressources/classic-light.png)


#### Sharp'Attack
```bash
./gnupy data/timewaveform.csv -t sharpattack
```
![sharpattack](ressources/sharpattack.png)