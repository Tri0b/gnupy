#!/usr/bin/env python

import argparse
import csv
from pathlib import Path
import os
import sys
from src.settings import Settings


def main(settings: Settings):

    if len(settings.fileList) > 0:
        settings.generateScript()
        outputDir = settings.outputDir
        scriptList = outputDir.glob("*.p")
        for script in scriptList:
            # Read to force to wait for popen to finish
            os.popen("gnuplot {}".format(script)).read()
            if settings.keepScript is False:
                script.unlink()  # Remove the gnuplot script


if __name__ == "__main__":
    settings = Settings()
    main(settings)
