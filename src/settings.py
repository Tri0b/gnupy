from pathlib import Path
from typing import List
import sys
import json


class Settings:
    def __init__(self):

        # Plot settings
        self.axisLabels = ["", ""]
        self.curveLabel = []
        self.x_column = 0  # Raw input
        self.y_columns = []  # Raw input
        self.ignoredColumns = []  # Raw input

        # Theme related
        self.theme = {}

        # csv related
        self.delimiter = ""
        self.quotechar = ""
        self.headers = []

        # File settings
        # Where the file graph is saved
        self.outputDir = Path()
        # In the future, this will be installed somewhere else
        self.rootDir = Path(__file__).parents[1]
        # dir from which the script is called
        self.currentDir = Path()
        # files to plot
        self.fileList = []
        self.fileExtention = ""
        self.keepScript = False

        # commands
        self.commands = []
        self.plotCommand = "plot"
        self.settingCommand = []
        self.extraArgs = []
        self.terminalCommand = ""

        self.fileList = []

    def setFiles(self, fileList):

        if not fileList:
            return

        for file in fileList:
            file = Path(file)
            if not file.is_file():
                print("{} is not a file".format(file))
            else:
                self.fileList.append(file)

    def setIgnore(self, ignoredColumns):

        if not ignoredColumns:
            return
        self.ignoredColumns = ignoredColumns

    def setOutputDir(self, outputDir):
        if not outputDir:
            self.outputDir = Path()
            return

        self.outputDir = Path(outputDir)
        if self.outputDir.is_file():
            print("{} is a file name")
            # parser.print_help()
            sys.exit(1)

        if not self.outputDir.exists():
            self.outputDir.mkdir(parents=True)

    def setFormat(self, fileExtention: any):
        self.fileExtention = fileExtention.replace(".", "").strip()
        # TODO: Create a json file where all formats and their settings are registred

        if self.fileExtention == "tex":
            terminal = "cairolatex"
            self.terminalCommand = "set terminal {} background rgb '{}'".format(
                terminal, self.theme["background"]
            )
        elif self.fileExtention == "svg":
            terminal = "svg"
        elif self.fileExtention == "jpg":
            terminal = "jpeg"
            self.terminalCommand = "set terminal {} enhanced truecolor large size 960,720 background rgb '{}'".format(
                terminal, self.theme["background"]
            )
        elif self.fileExtention == "png":
            terminal = "png"
            self.terminalCommand = "set terminal {} enhanced truecolor large size 960,720 background rgb '{}'".format(
                terminal, self.theme["background"]
            )
        else:
            print("{} is not a valid output format".format(self.fileExtention))
            # parser.print_help()
            sys.exit(1)

    def setDelimiter(self, delimiter):
        if not delimiter:
            return

        if delimiter in [",", ";", ".", "\t", "whitespace"]:
            self.delimiter = delimiter
        else:
            print("{} is not a valid delimiter".format(delimiter))
            exit(1)

    def setQuote(self, quote):
        if quote in ['"', "'"]:
            self.quotechar = quote
        else:
            print("{} is not a valid quote character".format(quote))
            # parser.print_help()
            exit(1)

    def setColumns(self, columns):

        if not columns:
            return
        self.y_columns = []
        for column in columns:
            self.y_columns.append(int(column))

    def getColumns(self, path: Path):

        nCol = self.getNColumns(path)
        columns = []
        if len(self.y_columns) == 0:
            columns = list(range(1, nCol + 1))
            columns.remove(self.x_column)  # Remove the x_column
            for ignoredCol in self.ignoredColumns:
                if ignoredCol in columns:
                    columns.remove(ignoredCol)
            return columns

        for col in self.y_columns:
            if col > nCol:
                print("Column {} does not exist".format(col))
            elif col in self.ignoredColumns:
                print("Column {} ignored".format(col))
            else:
                columns.append(col)
        return columns

    def getNColumns(self, path: Path):
        with open(path, "r") as file:
            line = file.readline().split(self.delimiter)
            return len(line)

    def setXAxis(self, x_column: int):
        # nCol must be updated before calling this function

        if not x_column:
            return
        self.x_column = x_column

    def setTheme(self, themeName: str):
        themeDict = {}
        for themeFile in self.getThemeFiles():
            themeDict[themeFile.stem] = themeFile

        if themeName not in themeDict:
            print("{} not found".format(themeName))
            themeName = "hope"

        with open(themeDict[themeName], "r") as file:
            self.theme = json.load(file)

        if "background" not in self.theme:
            self.theme["background"] = "#FFFFFF"

    def getThemeList(self):
        themeList = [theme.stem for theme in self.getThemeFiles()]
        return themeList

    def getThemeFiles(self):
        themeDir = Path(self.rootDir, "themes")
        themeFiles = [theme for theme in themeDir.glob("*.json")]
        return themeFiles

    def getHeaders(self, path: Path):

        if not path.is_file():
            print("{} is not a valid file path".format(path))
            return

        with open(path, "r") as file:
            headers = []
            line = file.readline().split(self.delimiter)
            nCol = len(line)
            # Check if the line has headers
            hasHeader = False
            for cell in line:
                # if at least one value is not numeric
                if cell.strip().isnumeric() is False:
                    hasHeader = True

            for index, cell in enumerate(line):
                cell = cell.strip()
                if cell == "" or hasHeader is False:
                    headers.append("y{}".format(index + 1))
                else:
                    headers.append(cell)
        return headers

    def generateScript(self):

        for file in self.fileList:
            self.commands = []
            title = file.stem

            # get all columns to plot
            nCols = self.getNColumns(file)
            columns = self.getColumns(file)
            # get column headers
            headers = self.getHeaders(file)

            # Check that x column exist for this file
            if self.x_column > nCols:
                print("Column {} does not exist in file {}".format(x_column, file))
                continue  # Skip this file

            n_color = len(self.theme["colors"])
            # create the plot command
            self.commands = []
            plotArguments = []
            for index, column in enumerate(columns):
                plotArguments.append(
                    "'{}' using {}:{} with lines title '{}' lt rgb '{}'".format(
                        file,
                        self.x_column,
                        column,
                        headers[index + 1],
                        self.theme["colors"][index % n_color],
                    )
                )
            plotArguments = ",".join(plotArguments)
            plotCommand = "{} {}".format(self.plotCommand, plotArguments)

            #        plotCommand = 'plot for [i=2:{}] {} using 1:i with lines title columnhead(i)'.format(n,metaStr(str(fileName)))
            outputFile = Path(
                self.outputDir, "{}.{}".format(file.stem, self.fileExtention)
            )
            scriptFile = Path(self.outputDir, "{}.p".format(file.stem))
            # gather all commands
            self.settingCommand = [
                "set autoscale",
                "set output '{}'".format(outputFile),
                "set xlabel '{}'".format(headers[self.x_column - 1]),
                "set datafile separator '{}'".format(self.delimiter),
                "set border linecolor rgb '{}'".format(self.theme["fontcolor"]),
                "set grid linecolor rgb '{}'".format(self.theme["fontcolor"]),
                "set xtics textcolor rgb '{}'".format(self.theme["fontcolor"]),
                "set ytics textcolor rgb '{}'".format(self.theme["fontcolor"]),
                "set title textcolor rgb '{}'".format(self.theme["fontcolor"]),
                "set xlabel textcolor rgb '{}'".format(self.theme["fontcolor"]),
                "set ylabel textcolor rgb '{}'".format(self.theme["fontcolor"]),
                "set key textcolor rgb '{}'".format(self.theme["fontcolor"]),
            ]
            commands = [self.terminalCommand, *self.settingCommand, plotCommand]

            # Write the gnuplot script
            with open(scriptFile, "w") as script:
                for line in commands:
                    script.write("{}\n".format(line))

    def plot(self, scriptFile: Path):

        # execute the gnuplot script
        os.popen("gnuplot {}".format(scriptFile))
